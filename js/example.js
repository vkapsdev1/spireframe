var global_folder_id = "root";
var temp_folder_id = "";
//Filesystem
window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;

//Here is the code to save things into file through file system
var appFS = null;
var requestStorage = 1024 * 1024;
var folderId;var ginfo;var authToken={};var auth_resp={};var parentId;var currentId;var Id;var data;var radiovalue;var sign;var gauth;var prevSign;var gtotal=0,gtaxamount=0,gcounter=0;function print_today(){var now=new Date();var months=new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');var date=((now.getDate()<10)?"0":"")+now.getDate();function fourdigits(number){return(number<1000)?number+1900:number}
var today=months[now.getMonth()]+" "+date+", "+(fourdigits(now.getYear()))+"-"+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();return today}
function print_year(){var now=new Date();function fourdigits(number){return(number<1000)?number+1900:number}
var year=fourdigits(now.getYear());return year}
function print_month(){var now=new Date();var months=new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');var mon=months[now.getMonth()];return mon}
function print_date(){var now=new Date();var date=((now.getDate()<10)?"0":"")+now.getDate();return date}
function roundNumber(number,decimals){var newString;decimals=Number(decimals);if(decimals<1){newString=(Math.round(number)).toString()}else{var numString=number.toString();if(numString.lastIndexOf(".")==-1){numString+="."}
var cutoff=numString.lastIndexOf(".")+decimals;var d1=Number(numString.substring(cutoff,cutoff+1));var d2=Number(numString.substring(cutoff+1,cutoff+2));if(d2>=5){if(d1==9&&cutoff>0){while(cutoff>0&&(d1==9||isNaN(d1))){if(d1!="."){cutoff-=1;d1=Number(numString.substring(cutoff,cutoff+1))}else{cutoff-=1}}}
d1+=1}
if(d1==10){numString=numString.substring(0,numString.lastIndexOf("."));var roundedNum=Number(numString)+1;newString=roundedNum.toString()+'.'}else{newString=numString.substring(0,cutoff)+d1.toString()}}
if(newString.lastIndexOf(".")==-1){newString+="."}
var decs=(newString.substring(newString.lastIndexOf(".")+1)).length;for(var i=0;i<decimals-decs;i++)newString+="0";return newString}
function update_total(){var subtotal=0;var total=0;var taxAmount=0;var tax=$("#taxes").val().replace("%","");$('.price').each(function(i){price=$(this).html();if(!isNaN(price)){subtotal+=Number(price);taxAmount=subtotal*(tax/100);total=subtotal+taxAmount}});total=roundNumber(total,2);taxAmount=roundNumber(taxAmount,2);gtotal=total;gtaxamount=taxAmount;$('#subtotal').html(subtotal);$('#total').html(total);$('#taxAmount').html(taxAmount);update_balance();localStorage.setItem('tax',tax)}
function update_balance(){
	var due=$("#total").html()-$("#paid").val();
	due=roundNumber(due,2);
	$('.due').html(due);
	update_balance_currency();
}
function update_price(){var row=$(this).parents('.item-row');var price=row.find('.cost').val()*row.find('.qty').val();price=roundNumber(price,2);isNaN(price)?row.find('.price').html("N/A"):row.find('.price').html(price);update_total()}
function update_item(){var row=$(this).parents('.item-row');var item_code=row.find('.item').val();for(var member of inventory){if(member[0]==item_code){row.find('.item-descr').html(member[1]);row.find('.cost').html(member[3]);row.find('.qty').html(1);var price=row.find('.cost').val()*row.find('.qty').val();price=roundNumber(price,2);isNaN(price)?row.find('.price').html("N/A"):row.find('.price').html(price);update_total();return}}}
function update_terms(){var terms=$("#terms-text").val();localStorage.setItem('terms',terms)}
function update_address(){var addr=$("#address").val();localStorage.setItem('address',addr)}
function update_vendor(){localStorage.setItem('vendor',$("#vendor-title").val())}
function update_counter(){var counter=$("#counter").val();gcounter=counter;localStorage.setItem('counter',counter)}
function update_taxes(){var tax=$("#taxes").html().replace("%","")}

function update_currency(){
	var v=$("select[name=currency]").val();
	localStorage.setItem('currency',v);
	update_balance_currency();
}


function update_balance_currency(){
	$("#sign").html(sign);
}

function change_currency(){
	var row=$(".cost").parents('.item-row');var t=row.find('.cost').val().replace(prevSign,sign);
}


function bind(){
	$(".cost").blur(update_price);
	$(".qty").blur(update_price);
	$(".item").blur(update_item);
	$("#address").blur(update_address);
	$("#vendor-title").blur(update_vendor);
	$("#counter").blur(update_counter);
	$("#taxes").blur(update_total);
	$("#terms-text").blur(update_terms);
}

function createPDF(){
	$('#addrow').hide();
	$('.delete').hide();
	getCanvas().then(function(canvas){
		img=canvas.toDataURL("image/png"),doc=new jsPDF({orientation:'p',unit:'px',format:'letter'});doc.addImage(img,'PNG',0,0);
		data=doc.output('blob');var p=[];
		mime="application/vnd.google-apps.folder";

		//get_or_create("BillingSnap Invoice",mime,p,function(id){
			folderId=global_folder_id;p.push(global_folder_id);mime="application/pdf";
			var file_name=print_today()+'-'+$('#customer-title').val().substring(0,16)+'-'+'BillingSnap';
			get_or_create(file_name,mime,p,save_pdf);
		//});

		$('#addrow').show();
		$('.delete').show();
	});
}

function printPDF(){
	window.print();
	/*getCanvas().then(function(canvas){
		img=canvas.toDataURL("image/jpeg"),chrome.tabs.create({'url':img},function(tab){});
	});*/
}

function getCanvas(){return html2canvas($('#page-wrap'))}

function get_or_create(name,mime,parent,callback){
	var created=0;
	gapi.client.request({'path':'https://www.googleapis.com/drive/v3/files','headers':{'Authorization':'Bearer '+authToken.access_token},'params':{'q':"name='"+name+"'"}}).then(function(response){
  
		if(response.result.files.length==0){ 
			created=1;
			gapi.client.request( {'path':'https://www.googleapis.com/drive/v3/files','headers':{'Authorization':'Bearer '+authToken.access_token},'method':'POST','body':{'name':name,'mimeType':mime,'parents':parent}}).then(function(resp){
					Id=resp.result.id;callback(Id,created);
					get_files_list();
				})
	}
		else{created=0;Id=response.result.files[0].id;callback(Id,created)}
	})
}

var folderListTree = [];
var folderListTreeIds = [];
var folderList = [];

function get_files_list(){
	console.log('global_folder_id  '+ global_folder_id);
	$('.listing').html('');
	console.log(folderList);
	console.log(global_folder_id);
	console.log(folderList[global_folder_id]);
	$('.folder_name_listing').html(folderList[global_folder_id]);
	gapi.client.request({'path':'https://www.googleapis.com/drive/v3/files','headers':{'Authorization':'Bearer '+authToken.access_token},'params':{'q':"mimeType='application/pdf' and parents='"+global_folder_id+"'",'fields': '*','pageToken':"",'pageSize':500 }}).then(function(response){
		console.log(response);
		if( typeof  response.result !== 'undefined' && response.result.files.length ) {

			for( var i=0; i < response.result.files.length; i++ ) {
				//console.log(response.result.files[i]);
				if( response.result.files[i].name.search("-BillingSnap") > 0 ){
					$('.listing').append('<a href="'+response.result.files[i].webViewLink+'" target="_blank">'+response.result.files[i].name+'</a> &nbsp; <a class="delete_file red-btn" href="javascript:;" title="Remove row" data-fileid="'+response.result.files[i].id+'">X</a> <br><br> ');
				}
			}

			$('#spinner').removeClass('loader');
			$('.block_screen').hide();
		}
	}).then(function(){
		if( $.trim($('.listing').html()) == "") {
			$('.listing').html('No invoices found!');
		}
	});
}

function get_folder_list(nextPageToken,  current_folder_name, current_folder_id, firstCall){

	//console.log(folderList.length);
	if( current_folder_id == "" ) {
		current_folder_id = 'root';
	}

//	console.log(' asdfasdfl ;lalsdf;lkas;dfkjjas;lkdf as');
	gapi.client.request({'path':'https://www.googleapis.com/drive/v3/files','headers':{'Authorization':'Bearer '+authToken.access_token},'params':{'q':"mimeType='application/vnd.google-apps.folder' and parents='"+current_folder_id+"'",'fields': 'files(id,name,parents,ownedByMe),nextPageToken','pageToken':nextPageToken ? nextPageToken : "",'pageSize':500 }}).then(function(response){

		//console.log('sdfsdf ' + response.nextPageToken);
		const o_token = response.nextPageToken;
		//console.log(response);
		//console.log(response);
		//console.log(response.result.files.length);

		if( typeof  response.result !== 'undefined' && response.result.files.length ) {
			if( current_folder_name == "") {

				for( var i=0; i < response.result.files.length; i++ ) {
				//	console.log(' here ');
				//	console.log(response.result.files[i]['name']);
					folderList[response.result.files[i]['id']] =  response.result.files[i]['name'];
					folderListTreeIds[folderListTree.length] = response.result.files[i]['id'];
					folderListTree[folderListTree.length] = response.result.files[i]['name'];

					//Adding into select box
					var isSelected = "";
					if( global_folder_id == response.result.files[i]['id']) {
						isSelected = "selected";
					}
					$('#invoice_folder_setting').append('<option '+isSelected+' value="'+response.result.files[i]['id']+'" >'+response.result.files[i]['name']+'</option>');

					get_folder_list("",  response.result.files[i]['name'], response.result.files[i]['id'], false);
					
				}

			} else {
			//	console.log(response.result.files.length);
			//	console.log(response.result);
				for( var i=0; i < response.result.files.length; i++ ) {
					
					folderList[response.result.files[i]['id']] = folderList[response.result.files[i].parents[0]] + " > " + response.result.files[i]['name'];
					folderListTreeIds[folderListTree.length] = response.result.files[i]['id'];
					folderListTree[folderListTree.length] = folderList[response.result.files[i].parents[0]] + " > " + response.result.files[i]['name'];

					//Adding into select box
					var isSelected = "";
					if( global_folder_id == response.result.files[i]['id']) {
						isSelected = "selected";
					}
					$('#invoice_folder_setting').append('<option '+isSelected+' value="'+response.result.files[i]['id']+'" >'+folderList[response.result.files[i].parents[0]] + " > " + response.result.files[i]['name']+'</option>');

					get_folder_list("",  response.result.files[i]['name'], response.result.files[i]['id'],false);

				}

			}
			
		}

		//Get existing invoices list at first page refresh
		if( firstCall){
			get_files_list();
		}

		$('.o_popup').hide();
		$('.block_screen').hide();
		$('#spinner').removeClass('loader');

		



	});
}

function read_sheet(id){
	var url='https://sheets.googleapis.com/v4/spreadsheets/'+id+'/values/Sheet1!A:D';mime="application/json";if(self.fetch){var setHeaders=new Headers();setHeaders.append('Authorization','Bearer '+auth_resp.access_token);setHeaders.append('Content-Type',mime);var setOptions={method:'GET',headers:setHeaders,credentials:'same-origin',};fetch(url,setOptions).then(response=>{if(response.ok){return response.json()}
else{console.log("Response wast not ok")}}).then(function(data){inventory=data.values}).catch(error=>{console.log("There is an error "+error.message)})}
}
function save_sheet(id,start,end,append,data,callback){
	var url='https://sheets.googleapis.com/v4/spreadsheets/'+id+'/values/Sheet1!'+start+':'+end+'?valueInputOption=USER_ENTERED';var type_of_request='PUT';if(append==1){url='https://sheets.googleapis.com/v4/spreadsheets/'+id+'/values/Sheet1!'+start+':'+end+':append?valueInputOption=USER_ENTERED';type_of_request='POST'}
mime="application/json";if(self.fetch){var setHeaders=new Headers();setHeaders.append('Authorization','Bearer '+auth_resp.access_token);setHeaders.append('Content-Type',mime);var body_data={'range':'Sheet1!'+start+':'+end,'majorDimension':'ROWS','values':[data]}
var setOptions={method:type_of_request,headers:setHeaders,credentials:'same-origin',body:JSON.stringify(body_data)};fetch(url,setOptions).then(response=>{if(response.ok){
	$('#spinner').removeClass('loader');
	$('.block_screen').hide();
}
else{}
callback()}).catch(error=>{})}
}
function save_pdf(id){
	var name=$('#counter').html();
	var today=print_today();
	var sheet_data=[];
	var mime="application/vnd.google-apps.spreadsheet";
	var p=[];current_id=id;var notID=0;var fileId=id;
	const url='https://www.googleapis.com/upload/drive/v3/files/'+fileId+'?uploadType=media';
	mime="application/pdf";
	if(self.fetch){
		var setHeaders=new Headers();setHeaders.append('Authorization','Bearer '+auth_resp.access_token);setHeaders.append('Content-Type',mime);var setOptions={method:'PATCH',headers:setHeaders,body:data};fetch(url,setOptions).then(response=>{if(response.ok){if(radiovalue=='cheque'){sheet_data=[today,gcounter,"",gtotal-gtaxamount,"",gtaxamount,gtotal]}
else if(radiovalue=='credit'){sheet_data=[today,gcounter,"","",gtotal-gtaxamount,gtaxamount,gtotal]}
else{sheet_data=[today,gcounter,gtotal-gtaxamount,"","",gtaxamount,gtotal]}
get_or_create(print_month()+"-"+print_year(),mime,p,function(Id){save_sheet(Id,'A1','G1',1,sheet_data,function(){$('#spinner').removeClass('loader'); $('.block_screen').hide(); alert("Great!!! Saved in Google Drive.")})})}
	else{

		alert("There is something wrong, Please remove and re-install extension.");
		window.close();
	} }).catch(error=>{});
	}
}



function done(){}
function printReceipt(divName){
 var printContents=document.getElementById(divName).innerHTML;var originalContent=document.body.innerHTML;document.body.innerHTML=printContents;window.print();
}



$(document).ready(function(){
	init();
	iconUrl = chrome.extension.getURL("img/small.png"); 
	console.log(iconUrl);
	$('#exe_logo').attr('src', iconUrl);

	
	$(".sel").focus(function(){
		$(this).select();
	});
	$('#currency_id').change(function(e){
		var optionSelected=$(this).find("option:selected");
		var valueSelected=optionSelected.val();
		sign=optionSelected.text();
		update_currency();
	});

$('[data-toggle="tooltip"]').tooltip();

$('#reset').click(function(){

	window.location.reload();

});

/*var cc=localStorage.getItem('currency');
$("#currency_id").val(cc).find("option[value="+cc+"]").attr('selected',!0);*/

var optionSel=$('#currency_id').val();
sign=optionSel;
update_balance_currency();
var vendor = localStorage.getItem('vendor');
var addr = localStorage.getItem('address');
var tax = localStorage.getItem('tax');

$('input[type=radio]').click(function(){
 radiovalue=$('input[name=mode_payment]:checked').val(); 
});

if(addr||addr===!1){$("#address").val(addr)}
if(vendor||vendor===!1){$("#vendor-title").val(vendor)}
if(tax||tax===!1){$("#taxes").val(tax+'%')}
var t=localStorage.getItem('terms');if(t||t===!1){$("#terms-text").val(t)}
var counter=0;counter=localStorage.getItem('counter');counter++;$("#counter").val(counter);

$("#create_pdf").click(function(){
	$('#spinner').addClass('loader');
	$('.block_screen').show();
	update_counter();
	$('body').scrollTop(0);
	createPDF();
});

$("#print_pdf").click(function(){ 
	update_counter();
	$('body').scrollTop(0);
	printPDF();
});



function viewDrive(id){
	var creat={'url':'https://drive.google.com/drive/folders/'+id} 
	chrome.tabs.create(creat,function(tab){});
}

$('input').click(function(){
	$(this).select();
});

$("#paid").blur(update_balance);
$("#addrow").click(function(){
	$(".item-row:last").after('<tr class="item-row"><td class="item-name"><textarea class="sel">Item Name</textarea></td><td class="description"><textarea class="sel">Description</textarea></td><td><textarea class="qty">0</textarea></td><td><textarea class="cost">0</textarea></td><td><div class="delete-wpr"><span class="price">0</span><a class="delete red-btn" href="javascript:;" title="Remove row">X</a></div></td></tr>');if($(".delete").length>0)$(".delete").show();bind();
});bind();

$("#items").on('click','.delete',function(){ $(this).parents('.item-row').remove();update_total();if($(".delete").length<2)$(".delete").hide()});$("#cancel-logo").click(function(){$("#logo").removeClass('edit')});$("#delete-logo").click(function(){$("#logo").remove()});$("#change-logo").click(function(){$("#logo").addClass('edit');$("#imageloc").val($("#image").attr('src'));$("#image").select()});$("#save-logo").click(function(){$("#image").attr('src',$("#imageloc").val());$("#logo").removeClass('edit')});$("#date").val(print_today())})



$( function() {

	//Datepicker installation on datepicker input field
    $( "#datepicker" ).datepicker();

    
    $('#settings').click(function(){

    	
    	$('.list_screen').hide();
    	$('.invoice_create_screen').hide(300, function(){
    		$('.setting_screen').show();
    		
    		$('.hide_list').hide();
    	});
    	
    });    

   $('#list').click(function(){

    	
    	$('.setting_screen').hide();
    	$('.invoice_create_screen').hide(300, function(){
    		$('.list_screen').show();
    		
    		$('.hide_list').hide();
    	});
    	
    });


   //Delete file
   $('body').on('click', '.delete_file', function(){

   	var targetId = $(this).data('fileid');
   	var confirm_flag = confirm('Are you sure you want to delete this file?');

   	if( confirm_flag ) {

   		$('#spinner').addClass('loader');
   		$('.block_screen').show();

   		gapi.client.request({'path':'https://www.googleapis.com/drive/v3/files/'+targetId,'headers':{'Authorization':'Bearer '+authToken.access_token},'params':{},'method':'DELETE', }).then(function(response){
		    console.log(response);
			get_files_list();
		});
   	}

   });

    $('#reset').click(function(){
    	
    	getSetting();
    	$('.setting_screen').hide(300, function(){

    		$('.invoice_create_screen').show();
    		$('.hide_list').show();
    	});
    	
    });

	function readArrayBuffer(buf) {
	    return new Uint8Array(buf);
	}


	//saving setting files
	$('#address_setting').focusout(function(){

		var address_setting_text = $.trim($(this).val());

		// Create Folder
	    appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
	        // Write settings to File System
	        dirEntry.getFile('/Settings/address_setting.txt', { create: true }, function (fileEntry) {
	            fileEntry.createWriter(function (fileWriter) {
	                // Delete file content first
	                fileWriter.truncate(0);

	                // Wait 500 milliseconds then save new content
	                setTimeout(function () {
	                    fileWriter.onwriteend = function (e) {
	                        //closePopup();
	                    };

	                    fileWriter.onerror = function (e) {
	                        console.log('Write failed: ' + e.toString());
	                    };

	                    // Create a new Blob and write it
	                    var blob = new Blob([address_setting_text], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
	                    fileWriter.write(blob);



	                }, 500);
	            }, fsErrorHandler);
	        }, fsErrorHandler);
	    });

	});

	$('#invoice_folder_setting').change(function(){

		var invoice_folder_setting_text = $.trim($(this).val());

		global_folder_id =  invoice_folder_setting_text;

		// Create Folder
	    appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
	        // Write settings to File System
	        dirEntry.getFile('/Settings/folder_setting.txt', { create: true }, function (fileEntry) {
	            fileEntry.createWriter(function (fileWriter) {
	                // Delete file content first
	                fileWriter.truncate(0);

	                // Wait 500 milliseconds then save new content
	                setTimeout(function () {
	                    fileWriter.onwriteend = function (e) {
	                        //closePopup();
	                    };

	                    fileWriter.onerror = function (e) {
	                        console.log('Write failed: ' + e.toString());
	                    };

	                    // Create a new Blob and write it
	                    var blob = new Blob([invoice_folder_setting_text], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
	                    fileWriter.write(blob);



	                }, 500);
	            }, fsErrorHandler);
	        }, fsErrorHandler);
	    });

	});

	$('#tax_setting').focusout(function(){

		var address_setting_text = $.trim($(this).val());

		// Create Folder
	    appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
	        // Write settings to File System
	        dirEntry.getFile('/Settings/tax_setting.txt', { create: true }, function (fileEntry) {
	            fileEntry.createWriter(function (fileWriter) {
	                // Delete file content first
	                fileWriter.truncate(0);

	                // Wait 500 milliseconds then save new content
	                setTimeout(function () {
	                    fileWriter.onwriteend = function (e) {
	                        //closePopup();
	                    };

	                    fileWriter.onerror = function (e) {
	                        console.log('Write failed: ' + e.toString());
	                    };

	                    // Create a new Blob and write it
	                    var blob = new Blob([address_setting_text], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
	                    fileWriter.write(blob);
	                }, 500);
	            }, fsErrorHandler);
	        }, fsErrorHandler);
	    });

	});

	$('#term_setting').focusout(function(){


		var address_setting_text = $.trim($(this).val());

		// Create Folder
	    appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
	        // Write settings to File System
	        dirEntry.getFile('/Settings/term_setting.txt', { create: true }, function (fileEntry) {
	            fileEntry.createWriter(function (fileWriter) {
	                // Delete file content first
	                fileWriter.truncate(0);

	                // Wait 500 milliseconds then save new content
	                setTimeout(function () {
	                    fileWriter.onwriteend = function (e) {
	                        //closePopup();
	                    };

	                    fileWriter.onerror = function (e) {
	                        console.log('Write failed: ' + e.toString());
	                    };

	                    // Create a new Blob and write it
	                    var blob = new Blob([address_setting_text], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
	                    fileWriter.write(blob);
	                }, 500);
	            }, fsErrorHandler);
	        }, fsErrorHandler);
	    });

	});


	//Add new folder 
	$('body').on('click', '.addFolder', function(){
		$('.o_popup').show();
		$('.block_screen').show();
		 

		temp_folder_id = $('#invoice_folder_setting').val();
	});
	$('body').on('click', '.block_screen', function(){
		if($('.o_popup').css('display') != 'none') {
			$('.o_popup').hide();
			$('.block_screen').hide();
		}
	});


	$('body').on('click', '.o_popup_btn', function(){
		
		var new_folder_name = $('#new_folder_name').val();
		if($.trim(new_folder_name) != ""){
			
			$('#spinner').addClass('loader');
			$('.o_popup').hide();

			var p=[];
            p.push(temp_folder_id);
            console.log(p);
            var mime="application/vnd.google-apps.folder"
        	get_or_create(new_folder_name,mime,p,function(Id){

        		global_folder_id = Id;
        		console.log(global_folder_id);

        		appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
				    // Write settings to File System
				    dirEntry.getFile('/Settings/folder_setting.txt', { create: true }, function (fileEntry) {
				        fileEntry.createWriter(function (fileWriter) {
				            // Delete file content first
				            fileWriter.truncate(0);

				            // Wait 500 milliseconds then save new content
				            setTimeout(function () {
				                fileWriter.onwriteend = function (e) {
				                    //closePopup();
				                };

				                fileWriter.onerror = function (e) {
				                    console.log('Write failed: ' + e.toString());
				                };

				                // Create a new Blob and write it
				                var blob = new Blob([Id], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
				                fileWriter.write(blob);


				                setTimeout(function(){ get_folder_list("","", "", false); }, 500);	

				            }, 500);
				        }, fsErrorHandler);
				    }, fsErrorHandler);
				});

        	});

		} else {
			$('#spinner').removeClass('loader');

			$('.o_popup').hide();
			$('.block_screen').hide();
		}

	});
	
});
//Here now getting setting from file system

	function createDefaultFolder(){
		console.log('sdfsdfsadfasdfasdfasdfasdf');
		var p=[];
    	var mime="application/vnd.google-apps.folder";
        get_or_create("BillingSnap Invoice",mime,p,function(Id){

        	global_folder_id = Id;
        	console.log('Default folder');
        	console.log('global_folder_id '+global_folder_id);
        	

        	appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
			    // Write settings to File System
			    dirEntry.getFile('/Settings/folder_setting.txt', { create: true }, function (fileEntry) {
			        fileEntry.createWriter(function (fileWriter) {
			            // Delete file content first
			            fileWriter.truncate(0);

			            // Wait 500 milliseconds then save new content
			            setTimeout(function () {
			                fileWriter.onwriteend = function (e) {
			                    //closePopup();
			                };

			                fileWriter.onerror = function (e) {
			                    console.log('Write failed: ' + e.toString());
			                };

			                // Create a new Blob and write it
			                var blob = new Blob([Id], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
			                fileWriter.write(blob);


			                setTimeout(function(){ get_folder_list("", "", "", false); }, 500);	

			            }, 500);
			        }, fsErrorHandler);
			    }, fsErrorHandler);
			});

        

		});
	}
	function getSetting(){

		appFS.root.getDirectory('Settings', { create: true }, function (dirEntry) {
	        
	        dirEntry.getFile('/Settings/folder_setting.txt', {}, function (fileEntry) {
	            fileEntry.file(function (file) {

	            	console.log('Hereasdfas');
	                var readFile = new FileReader();
	                readFile.onloadend = function (e) {
	                	//console.log(this.result);
	                	var folderId = this.result;
	                	console.log('folderId '+ folderId);
	                	if($.trim(folderId) != "") {
	                		global_folder_id = folderId;
	                	} else {
	                		createDefaultFolder();
	                	}
	                	/*var p=[];
			            p.push(folderId);
			            var mime="application/vnd.google-apps.folder";
	                	get_or_create("BillingSnap Invoice",mime,p,function(Id){
			            	

							

						});*/

	                   
	                };

	                readFile.readAsText(file);
	            });
	        }, function() {
	        	createDefaultFolder();
	        });

	        dirEntry.getFile('/Settings/address_setting.txt', {}, function (fileEntry) {
	            fileEntry.file(function (file) {
	                var readFile = new FileReader();
	                readFile.onloadend = function (e) {

	                	$('#address_setting').val(this.result);

	                	$('#address').val(this.result);
	                   
	                };

	                readFile.readAsText(file);
	            });
	        });

	        dirEntry.getFile('/Settings/tax_setting.txt', {}, function (fileEntry) {
	            fileEntry.file(function (file) {
	                var readFile = new FileReader();
	                readFile.onloadend = function (e) {

	                	$('#tax_setting').val(this.result);
	                	$('#taxes').val(this.result);

	                    
	                };

	                readFile.readAsText(file);
	            });
	        });

	        dirEntry.getFile('/Settings/term_setting.txt', {}, function (fileEntry) {
	            fileEntry.file(function (file) {
	                var readFile = new FileReader();
	                readFile.onloadend = function (e) {

	                	$('#term_setting').val(this.result);
	                	$('#terms-text').val(this.result);

	                    
	                };

	                readFile.readAsText(file);
	            });
	        });

	    });

	}
	//end of getSetting function

function onInitFs(fs) {
appFS = fs;
getSetting();
}

// FileSystem Error Handler
	function fsErrorHandler(e) {
	    var msg = 'An error occured:';
	    console.log('error name = ', e.name);

	    switch (e.name) {
	        case 'QuotaExceededError':
	            msg += 'QUOTA_EXCEEDED_ERR';
	            break;
	        case 'NotFoundError':
	            msg += 'NOT_FOUND_ERR';
	            break;
	        case 'SecurityError':
	            msg += 'SECURITY_ERR';
	            break;
	        case 'InvalidModificationError':
	            msg += 'INVALID_MODIFICATION_ERR';
	            break;
	        case 'InvalidStateError':
	            msg += 'INVALID_STATE_ERR';
	            break;
	        default:
	            msg += 'Unknown Error';
	            break;
	    }
	    console.log('Error: ' + msg);
	}

function init(callback){
	getToken();function getToken(){ chrome.identity.getAuthToken({interactive:!0},function(t){if(chrome.runtime.lastError){alert(chrome.runtime.lastError.message);return}
authToken.access_token=t;auth_resp.access_token=t;function start(){gapi.client.load("drive","v3",function(){

	gapi.client.request({'path':'https://www.googleapis.com/drive/v3/files','headers':{'Authorization':'Bearer '+t}}).then(function(resp){if(resp.code==401){chrome.identity.removeCachedAuthToken({'token':t},function(){getToken()})}});


mime="application/vnd.google-apps.folder";var p=[];
//get_or_create("BillingSnap Invoice",mime,p,function(Id){});


$('#spinner').addClass('loader');
$('.block_screen').show();
//Calling Gapis for folders list
setTimeout(function(){ get_folder_list("", "", "", true); }, 100);
//setTimeout(function(){ get_files_list(); }, 300);
setTimeout(function(){ 
$('#spinner').removeClass('loader');
$('.block_screen').hide(); }, 2000);
setTimeout(function(){ 


	// FileSystem RequestQutota
	navigator.webkitPersistentStorage.requestQuota(
	    requestStorage,
	    function (grantedBytes) {
	        window.requestFileSystem(PERSISTENT, grantedBytes, onInitFs, fsErrorHandler);
	        // console.log('request file system storage');
     
	    },
	    function (e) {
	        console.log('Error', e);
	    }
	);


}, 280);		




 } ) }
gapi.load('client',start)}) 
console.log('sxdfsdf');
}
}

$(document).ready(function(){
	init();
});